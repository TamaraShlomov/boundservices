package il.co.hyperactive.boundservices;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class MyService extends Service {

    private IBinder myBinder = new MyBinder();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public class MyBinder extends Binder
    {
        MyService getService()
        {
            return MyService.this;
        }
    }

    void printSomething()
    {
        Log.i("MyService","This is the service talking!");
    }
}
